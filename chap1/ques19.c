#include <stdio.h>

f (struct emp);

struct emp
{
	char arr[10];
	int age;
};
int main()
{
	struct emp e = { "soldier",34};
	f(e);
}

f(struct emp a)
{
	printf("%s %d\n",a.arr,a.age);

}

//error occur in this because f declaration which uses struct appear before
//structure definition , shifting struct before f declaration will 
//resolve the issue
