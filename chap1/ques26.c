#include <stdio.h>

union a
{
	int a;
	char ch[2];
};
int main()
{
	union a test = 34;
	printf("%d %d \n",test.ch[0],test.ch[1]);

	return 0;
}
